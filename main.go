package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jasonlvhit/gocron"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"main/internal/controllers"
	"main/internal/models"
	"main/internal/services"

	"net/http"
)

var err error

func initialMigration(param string) {
	if param == "localhost" {
		models.DB, err = gorm.Open(mysql.Open(models.DNS), &gorm.Config{})
	}
	if param == "cuttieflame" {
		models.DB, err = gorm.Open(mysql.Open(models.DNS2), &gorm.Config{})
	}
	if err != nil {
		fmt.Println(err.Error())
		panic("Cannot conect to DB")
	}
	//включение миграций(не включать)
	//DB.AutoMigrate(&User{})
}
func cron() {
	gocron.Every(2).Hours().Do(services.ReservationCheck)
	<-gocron.Start()
}
func initializeRouter() {
	r := mux.NewRouter()

	r.HandleFunc("/users", controllers.GetUsers).Methods("GET")
	r.HandleFunc("/users/{id}", controllers.GetUser).Methods("GET")
	r.HandleFunc("/users", controllers.CreateUser).Methods("POST")
	r.HandleFunc("/users/{id}", controllers.UpdateUser).Methods("PUT")
	r.HandleFunc("/users/{id}", controllers.DeleteUser).Methods("DELETE")
	r.HandleFunc("/start_queue", controllers.StartCron).Methods("GET")
	r.HandleFunc("/stop_queue", controllers.StopCron).Methods("GET")
	r.HandleFunc("/start_tg", controllers.StartTg).Methods("GET")

	http.ListenAndServe(":9000", r)
}
func start() {

}
func main() {
	initialMigration("localhost")
	initializeRouter()
}
