module main

go 1.18

//require github.com/gorilla/mux v1.8.0 // indirect

require (
	github.com/gorilla/mux v1.8.0
	github.com/jasonlvhit/gocron v0.0.1
	gorm.io/driver/mysql v1.4.3
	gorm.io/gorm v1.24.1
)

require (
	github.com/Syfaro/telegram-bot-api v4.6.4+incompatible // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
