package models

import (
	"gorm.io/gorm"
)

var DB *gorm.DB
var err error

const DNS = "root:root@tcp(127.0.0.1:33062)/aftwrk?charset=utf8mb4&parseTime=True&loc=Local"
const DNS2 = "username:password@tcp(95.183.13.21:3306)/aftwrk?charset=utf8mb4&parseTime=True&loc=Local"

type User struct {
	gorm.Model
	UserId          int    `json:"user_id"`
	Name            string `json:"name"`
	Email           string `json:"email"`
	EmailVerifiedAt string `json:"email_verified_at"`
	Password        string `json:"password"`
	CreatedAt       string `json:"created_at"`
	UpdatedAt       string `json:"updated_at"`
}
type Order struct {
	gorm.Model
	OrderId           int            `json:"order_id"`
	UserId            int            `json:"user_id"`
	OrderPaidComplete int            `json:"order_paid_complete"`
	OrderPaidCanceled int            `json:"order_paid_canceled"`
	OrderType         string         `json:"order_type"`
	PayUntil          string         `json:"pay_until"`
	LastUpdate        string         `json:"last_update"`
	Comment           string         `json:"comment"`
	Status            string         `json:"status"`
	IsGift            string         `json:"is_gift"`
	BuyerName         string         `json:"buyer_name"`
	BuyerEmail        string         `json:"buyer_email"`
	Created           string         `json:"created"`
	DeletedAt         gorm.DeletedAt `json:"deleted_at"`
}
type Reservation struct {
	gorm.Model
	ReservationId int    `json:"reservation_id"`
	UserId        int    `json:"user_id"`
	TableId       int    `json:"table_id"`
	Info          string `json:"info"`
	RestaurantId  int    `json:"restaurant_id"`
	DeletedAt     string `json:"deleted_at"`
	CreatedAt     string `json:"created_at"`
	UpdatedAt     string `json:"updated_at"`
}
type RestaurantTable struct {
	gorm.Model
	RestaurantId string `json:"restaurant_id"`
	TableId      string `json:"table_id"`
	Price        int    `json:"price"`
	Places       int    `json:"places"`
	IsActive     int    `json:"is_active"`
	Status       int    `json:"status"`
	CreatedAt    string `json:"created_at"`
	UpdatedAt    string `json:"updated_at"`
}
type OrderReservation struct {
	gorm.Model
	OrderId       int            `json:"order_id"`
	TableId       int            `json:"table_id"`
	Quantity      int            `json:"quantity"`
	Created       string         `json:"created"`
	RestaurantId  int            `json:"restaurant_id"`
	ReservationId int            `json:"reservation_id"`
	DeletedAt     gorm.DeletedAt `json:"deleted_at"`
}
