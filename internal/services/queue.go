package services

import (
	"main/internal/models"
	"time"
)

func ReservationCheck() {
	var reservations []models.Reservation
	models.DB.Find(&reservations)
	var tableIds []int
	for _, value := range reservations {
		tableIds = append(tableIds, value.TableId)
	}
	//fmt.Println(tableIds)
	models.DB.Model(models.RestaurantTable{}).Where("table_id IN ?", tableIds).Update("is_active", 1)

	//temperature := map[string][]services.Reservation{
	//	"Земля": reservations, // Пары ключ-значения являются композитными литералами для карт
	//}
	//data := map[string][]services.RestaurantTable{
	//	"Земля": restaurantTable, // Пары ключ-значения являются композитными литералами для карт
	//}
}
func UpdateStatusOrderReservation() {
	var orderReservations []models.OrderReservation
	models.DB.Where("updated_at > ?", time.Now().Format("2006-01-02")).Find(&orderReservations)
	var tableIds []int
	for _, value := range orderReservations {
		tableIds = append(tableIds, value.TableId)
	}
	models.DB.Model(models.RestaurantTable{}).Where("table_id IN ?", tableIds).Where("status = ?", 0).Update("status", 2)
}
